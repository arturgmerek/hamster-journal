import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { combineReducers, compose, applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

export const history = createBrowserHistory();

export const rootReducer = combineReducers({
  router: connectRouter(history),
});

const devMode = process.env.DEV_MODE === 'true';
const devToolsExtensionKey = '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__';

const composeEnhancers = devMode && devToolsExtensionKey in window ? (window as any)[devToolsExtensionKey] : compose;

export const middlewares = composeEnhancers(applyMiddleware(routerMiddleware(history), thunk));

export type AppState = ReturnType<typeof rootReducer>;

export default createStore(rootReducer, middlewares);
