import React from 'react';
import { render } from '@testing-library/react';

import App from './App';

test('renders hamster journal text', () => {
  const renderResult = render(<App />);
  const divElement = renderResult.getByText(/hamster journal/i);
  expect(divElement).toBeInTheDocument();
});
