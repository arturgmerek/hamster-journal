import React, { useState } from 'react';
import { Provider } from 'react-redux';

import ChoiceWheel, { ChoiceWheelTypes } from '../../components/ChoiceWheel';
import Hamster, { HamsterType } from '../../components/Hamster';
import Signifier, { SignifierType } from '../../components/Signifier';
import store from '../../redux/store';

import './styles.scss';

interface LogEntry {
  hamsterType?: HamsterType;
  signifierType?: SignifierType;
  text?: string;
}

const App = () => {
  const [entries, setEntries] = useState<LogEntry[]>([]);

  const handleHamsterChoiceClick = (hamsterType: HamsterType, index: number) => {
    setEntries(entries.map((entry, entryIndex) => {
      if (entryIndex !== index) {
        return entry;
      } else {
        return {
          ...entry,
          hamsterType,
        };
      }
    }));
  };

  const handleSignifierChoiceClick = (signifierType: SignifierType, index: number) => {
    setEntries(entries.map((entry, entryIndex) => {
      if (entryIndex !== index) {
        return entry;
      } else {
        return {
          ...entry,
          signifierType,
        };
      }
    }));
  };

  const addNewEntry = () => {
    setEntries([...entries, {}]);
  };

  const entriesElements = entries.map((entry, index) => {
    const onHamsterChoiceClick = (hamsterType: HamsterType | SignifierType) => {
      handleHamsterChoiceClick(hamsterType as HamsterType, index);
    };

    const onSignifierChoiceClick = (signifierType: HamsterType | SignifierType) => {
      handleSignifierChoiceClick(signifierType as SignifierType, index);
    };

    return (
      <div className="entry" key={index}>
        <ChoiceWheel type={ChoiceWheelTypes.SignifierChoices} onChoiceClick={onSignifierChoiceClick}>
          <Signifier type={entry.signifierType}/>
        </ChoiceWheel>
        <ChoiceWheel type={ChoiceWheelTypes.HamsterChoices} onChoiceClick={onHamsterChoiceClick}>
          <Hamster type={entry.hamsterType}/>
        </ChoiceWheel>
        <input/>
      </div>
    );
  });

  return (
    <Provider store={store}>
      <div className="App">
        <h1>Hamster Journal</h1>
        <div>
          {entriesElements}
        </div>
        <div onClick={addNewEntry}>
          Add hamster
        </div>
      </div>
    </Provider>
  );
};

export default App;
