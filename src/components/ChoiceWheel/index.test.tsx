import React from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';

import ChoiceWheel, { ChoiceWheelProps, ChoiceWheelTypes } from '.';
import { HamsterType } from '../Hamster';
import { SignifierType } from '../Signifier';

describe('ChoiceWheel', () => {
  let renderResult: RenderResult;

  const renderComponent = (props: ChoiceWheelProps) => {
    renderResult = render(<ChoiceWheel {...props}/>);
  }

  test('renders no options until clicked and only ? sign', () => {
    renderComponent({onChoiceClick: jest.fn(), type: ChoiceWheelTypes.HamsterChoices});
    expect(renderResult.getAllByText(/.+/i).length).toBe(1);
  });

  test('renders all hamster options', () => {
    renderComponent({onChoiceClick: jest.fn(), type: ChoiceWheelTypes.HamsterChoices});
    fireEvent.click(renderResult.getByText('?'));
    expect(renderResult.getAllByText(/.+/i).length).toBe(Object.keys(HamsterType).length + 1);
  });

  test('renders all signifier options', () => {
    renderComponent({onChoiceClick: jest.fn(), type: ChoiceWheelTypes.SignifierChoices});
    fireEvent.click(renderResult.getByText(/.*/g, {selector: '.chosen-one'}));
    expect(renderResult.getAllByText(/.+/i).length).toBe(Object.keys(SignifierType).length);
  });

  test('returns corresponding HamsterType value of a clicked hamster', () => {
    const onChoiceClick = jest.fn();
    renderComponent({onChoiceClick, type: ChoiceWheelTypes.HamsterChoices});
    fireEvent.click(renderResult.getByText('?'));
    fireEvent.click(renderResult.getByTitle(/task incomplete/i));
    expect(onChoiceClick).toHaveBeenCalled();
    expect(onChoiceClick).toHaveBeenCalledWith(HamsterType.TaskIncomplete);
  });

  test('returns corresponding SignifierType value of a clicked signifier', () => {
    const onChoiceClick = jest.fn();
    renderComponent({onChoiceClick, type: ChoiceWheelTypes.SignifierChoices});
    fireEvent.click(renderResult.getByText(/.*/g, {selector: '.chosen-one'}));
    fireEvent.click(renderResult.getByTitle(/important/i));
    expect(onChoiceClick).toHaveBeenCalled();
    expect(onChoiceClick).toHaveBeenCalledWith(SignifierType.Important);
  });
});
