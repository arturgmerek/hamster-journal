import React, { useState } from 'react';

import Hamster, { HamsterType } from '../Hamster';
import Signifier, { SignifierType } from '../Signifier';

import './styles.scss';

export enum ChoiceWheelTypes {
  HamsterChoices = 'Hamster Choices',
  SignifierChoices = 'Signifier Choices',
}

interface OwnProps {
  onChoiceClick: (type: HamsterType | SignifierType) => void;
  type: ChoiceWheelTypes;
}

export type ChoiceWheelProps = OwnProps;

const typeToTypesMapping = {
  [ChoiceWheelTypes.HamsterChoices]: HamsterType,
  [ChoiceWheelTypes.SignifierChoices]: SignifierType,
};
const typeToComponentMapping = {
  [ChoiceWheelTypes.HamsterChoices]: Hamster,
  [ChoiceWheelTypes.SignifierChoices]: Signifier,
};

const ChoiceWheel: React.FC<ChoiceWheelProps> = (props) => {
  const [choicesVisible, setChoicesVisible] = useState<boolean>(false);

  const handleChosenOneClick = () => {
    setChoicesVisible(!choicesVisible);
  };

  const wheelElements = (Object.values(typeToTypesMapping[props.type])).map((choice, index) => {
    const Component = typeToComponentMapping[props.type];
    const onClick = () => {
      props.onChoiceClick(choice);
      setChoicesVisible(!choicesVisible);
    };
    return (
      <div className={`choices-${props.type === ChoiceWheelTypes.HamsterChoices ? '8' : '3'}`} key={index} onClick={onClick}>
        <Component type={choice}/>
      </div>
    );
  });

  return (
    <div className={`choice-wheel${choicesVisible ? ' expand-bg' : ''}`}>
      <div className="chosen-one" onClick={handleChosenOneClick}>
        {props.children ?? props.type === ChoiceWheelTypes.HamsterChoices ? '?' : ' '}
      </div>
      <div className={`choices${choicesVisible ? ' visible' : ''}`}>
        {choicesVisible ?wheelElements : null}
      </div>
    </div>
  );
};

export default ChoiceWheel;
