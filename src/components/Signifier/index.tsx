import React from 'react';

import './styles.scss';

export enum SignifierType {
  Idea = 'Idea',
  Delete = 'Delete',
  Important = 'Important',
}

interface OwnProps {
  type?: SignifierType;
}

export type SignifierProps = OwnProps;

const Signifier: React.FC<SignifierProps> = (props) => {
  const typeToIconMapping = {
    [SignifierType.Idea]: '!',
    [SignifierType.Delete]: 'D',
    [SignifierType.Important]: '*',
  };

  return (
    <abbr className="signifier" title={props.type}>
      {props.type ? typeToIconMapping[props.type] : ' '}
    </abbr>
  );
};

export default Signifier;
