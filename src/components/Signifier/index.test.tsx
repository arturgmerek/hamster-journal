import React from 'react';
import { render, RenderResult } from '@testing-library/react';

import Signifier, { SignifierProps, SignifierType } from '.';

describe('Signifier', () => {
  let renderResult: RenderResult;

  const renderComponent = (props: SignifierProps) => {
    renderResult = render(<Signifier {...props}/>);
  }

  test('renders empty signifier when no type is passed', () => {
    renderComponent({});
    expect(renderResult.queryAllByText(/\w/i).length).toBe(0);
  });

  test('renders idea signifier', () => {
    renderComponent({type: SignifierType.Idea});
    expect(renderResult.getByText(/!/i)).toBeVisible();
  });

  test('renders idea signifier', () => {
    renderComponent({type: SignifierType.Important});
    expect(renderResult.getByText(/\*/i)).toBeVisible();
  });
});
