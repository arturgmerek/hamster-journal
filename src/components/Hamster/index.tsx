import React from 'react';

import './styles.scss';

export enum HamsterType {
  Note = 'Note',
  Event = 'Event',
  TaskIncomplete = 'Task Incomplete',
  TaskMigrated = 'Task Migrated',
  TaskScheduled = 'Task Scheduled',
  TaskComplete = 'Task Complete',
  TaskIrrevelant = 'Task Irrevelant',
  Delete = 'Delete',
}

interface OwnProps {
  type?: HamsterType;
}

export type HamsterProps = OwnProps;

const Hamster: React.FC<HamsterProps> = (props) => {
  const typeToIconMapping = {
    [HamsterType.Event]: 'o',
    [HamsterType.Note]: '-',
    [HamsterType.TaskComplete]: 'x',
    [HamsterType.TaskIncomplete]: '\u25CF',
    [HamsterType.TaskIrrevelant]: '\u25CF',
    [HamsterType.TaskMigrated]: '>',
    [HamsterType.TaskScheduled]: '<',
    [HamsterType.Delete]: 'D',
  };

  return (
    <abbr className="hamster" style={props.type === HamsterType.TaskIrrevelant ? {textDecoration: 'line-through'} : {}} title={props.type}>
      {props.type ? typeToIconMapping[props.type] : '?'}
    </abbr>
  );
};

export default Hamster;
