import React from 'react';
import { render, RenderResult } from '@testing-library/react';

import Hamster, { HamsterProps, HamsterType } from '.';

describe('Hamster', () => {
  let renderResult: RenderResult;

  const renderComponent = (props: HamsterProps) => {
    renderResult = render(<Hamster {...props}/>);
  }

  test('renders incomplete task', () => {
    renderComponent({type: HamsterType.TaskIncomplete});
    expect(renderResult.getByText(/\u25CF/i)).toBeVisible();
  });

  test('renders irrevelant task with line-through', () => {
    renderComponent({type: HamsterType.TaskIrrevelant});
    expect(renderResult.getByText(/\u25CF/i)).toHaveStyle('text-decoration: line-through');
  });

  test('renders completed task without line-through', () => {
    renderComponent({type: HamsterType.TaskComplete});
    expect(renderResult.getByText(/x/i)).not.toHaveStyle('text-decoration: line-through');
  });

  test('renders empty hamster when no type is passed', () => {
    renderComponent({});
    expect(renderResult.queryAllByText(/\w/i).length).toBe(0);
  });
});
