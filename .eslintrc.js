module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    "extends": ["eslint:recommended", "plugin:react/recommended"],
    "ignorePatterns": ["*.test.*"],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "project": "tsconfig.json",
        "sourceType": "module"
    },
    "plugins": [
        "@typescript-eslint",
        "@typescript-eslint/eslint-plugin-tslint",
        "react",
        "import",
        "prefer-arrow",
        "react-hooks"
    ],
    "rules": {
        "array-callback-return": [
            "warn"
        ],
        "default-case": [
            "warn",
            {
                "commentPattern": "^no default$"
            }
        ],
        "dot-location": [
            "warn",
            "property"
        ],
        "eqeqeq": [
            "error",
            "smart"
        ],
        "new-parens": "error",
        "no-array-constructor": [
            "warn"
        ],
        "no-caller": "error",
        "no-cond-assign": "error",
        "no-const-assign": [
            "warn"
        ],
        "no-control-regex": [
            "warn"
        ],
        "no-delete-var": [
            "warn"
        ],
        "no-dupe-args": [
            "warn"
        ],
        "no-dupe-class-members": [
            "warn"
        ],
        "no-dupe-keys": [
            "warn"
        ],
        "no-duplicate-case": "error",
        "no-empty-character-class": [
            "warn"
        ],
        "no-empty-pattern": [
            "warn"
        ],
        "no-eval": "error",
        "no-ex-assign": [
            "warn"
        ],
        "no-extend-native": [
            "warn"
        ],
        "no-extra-bind": "error",
        "no-extra-label": [
            "warn"
        ],
        "no-fallthrough": "off",
        "no-func-assign": [
            "warn"
        ],
        "no-implied-eval": [
            "warn"
        ],
        "no-invalid-regexp": [
            "warn"
        ],
        "no-iterator": [
            "warn"
        ],
        "no-label-var": [
            "warn"
        ],
        "no-labels": [
            "warn",
            {
                "allowLoop": true,
                "allowSwitch": false
            }
        ],
        "no-lone-blocks": [
            "warn"
        ],
        "no-loop-func": [
            "warn"
        ],
        "no-mixed-operators": [
            "warn",
            {
                "groups": [
                    [
                        "&",
                        "|",
                        "^",
                        "~",
                        "<<",
                        ">>",
                        ">>>"
                    ],
                    [
                        "==",
                        "!=",
                        "===",
                        "!==",
                        ">",
                        ">=",
                        "<",
                        "<="
                    ],
                    [
                        "&&",
                        "||"
                    ],
                    [
                        "in",
                        "instanceof"
                    ]
                ],
                "allowSamePrecedence": false
            }
        ],
        "no-multi-str": [
            "warn"
        ],
        "no-native-reassign": [
            "warn"
        ],
        "no-negated-in-lhs": [
            "warn"
        ],
        "no-new-func": "error",
        "no-new-object": [
            "warn"
        ],
        "no-new-symbol": [
            "warn"
        ],
        "no-new-wrappers": "error",
        "no-obj-calls": [
            "warn"
        ],
        "no-octal": [
            "warn"
        ],
        "no-octal-escape": [
            "warn"
        ],
        "no-redeclare": "error",
        "no-regex-spaces": [
            "warn"
        ],
        "no-restricted-syntax": [
            "warn",
            "WithStatement"
        ],
        "no-script-url": [
            "warn"
        ],
        "no-self-assign": [
            "warn"
        ],
        "no-self-compare": [
            "warn"
        ],
        "no-sequences": "error",
        "no-shadow-restricted-names": [
            "warn"
        ],
        "no-sparse-arrays": "error",
        "no-template-curly-in-string": "error",
        "no-this-before-super": [
            "warn"
        ],
        "no-throw-literal": "error",
        "no-undef": [
            "error"
        ],
        "no-restricted-globals": [
            "error",
            "addEventListener",
            "blur",
            "close",
            "closed",
            "confirm",
            "defaultStatus",
            "defaultstatus",
            "event",
            "external",
            "find",
            "focus",
            "frameElement",
            "frames",
            "history",
            "innerHeight",
            "innerWidth",
            "length",
            "location",
            "locationbar",
            "menubar",
            "moveBy",
            "moveTo",
            "name",
            "onblur",
            "onerror",
            "onfocus",
            "onload",
            "onresize",
            "onunload",
            "open",
            "opener",
            "opera",
            "outerHeight",
            "outerWidth",
            "pageXOffset",
            "pageYOffset",
            "parent",
            "print",
            "removeEventListener",
            "resizeBy",
            "resizeTo",
            "screen",
            "screenLeft",
            "screenTop",
            "screenX",
            "screenY",
            "scroll",
            "scrollbars",
            "scrollBy",
            "scrollTo",
            "scrollX",
            "scrollY",
            "self",
            "status",
            "statusbar",
            "stop",
            "toolbar",
            "top"
        ],
        "no-unreachable": [
            "warn"
        ],
        "no-unused-expressions": "error",
        "no-unused-labels": "error",
        "no-unused-vars": 0,
        "no-use-before-define": [
            "warn",
            {
                "functions": false,
                "classes": false,
                "variables": false
            }
        ],
        "no-useless-computed-key": [
            "warn"
        ],
        "no-useless-concat": [
            "warn"
        ],
        "no-useless-constructor": [
            "warn"
        ],
        "no-useless-escape": [
            "warn"
        ],
        "no-useless-rename": [
            "warn",
            {
                "ignoreDestructuring": false,
                "ignoreImport": false,
                "ignoreExport": false
            }
        ],
        "no-with": [
            "warn"
        ],
        "no-whitespace-before-property": [
            "warn"
        ],
        "react-hooks/exhaustive-deps": [
            "warn"
        ],
        "require-yield": [
            "warn"
        ],
        "rest-spread-spacing": [
            "warn",
            "never"
        ],
        "strict": [
            "warn",
            "never"
        ],
        "unicode-bom": [
            "warn",
            "never"
        ],
        "use-isnan": "error",
        "valid-typeof": "off",
        "no-restricted-properties": [
            "error",
            {
                "object": "require",
                "property": "ensure",
                "message": "Please use import() instead. More info: https://facebook.github.io/create-react-app/docs/code-splitting"
            },
            {
                "object": "System",
                "property": "import",
                "message": "Please use import() instead. More info: https://facebook.github.io/create-react-app/docs/code-splitting"
            }
        ],
        "getter-return": [
            "warn"
        ],
        "import/first": [
            "error"
        ],
        "import/no-amd": [
            "error"
        ],
        "import/no-webpack-loader-syntax": [
            "error"
        ],
        "react-hooks/rules-of-hooks": [
            "error"
        ],
        "quotes": "off",
        "@typescript-eslint/adjacent-overload-signatures": "error",
        "@typescript-eslint/array-type": "error",
        "@typescript-eslint/ban-types": "error",
        "@typescript-eslint/class-name-casing": "error",
        "@typescript-eslint/consistent-type-assertions": "error",
        "@typescript-eslint/consistent-type-definitions": "error",
        "@typescript-eslint/explicit-member-accessibility": [
            "off",
            {
                "accessibility": "explicit"
            }
        ],
        "@typescript-eslint/indent": [
            "error",
            2,
            {
                "ObjectExpression": "first",
                "FunctionDeclaration": {
                    "parameters": "first"
                },
                "FunctionExpression": {
                    "parameters": "first"
                },
                "SwitchCase": 1
            }
        ],
        "@typescript-eslint/interface-name-prefix": "error",
        "@typescript-eslint/member-delimiter-style": [
            "error",
            {
                "multiline": {
                    "delimiter": "semi",
                    "requireLast": true
                },
                "singleline": {
                    "delimiter": "semi",
                    "requireLast": false
                }
            }
        ],
        "@typescript-eslint/no-empty-function": "error",
        "@typescript-eslint/no-empty-interface": "error",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-misused-new": "error",
        "@typescript-eslint/no-namespace": "error",
        "no-param-reassign": "error",
        "@typescript-eslint/no-parameter-properties": "off",
        "@typescript-eslint/no-this-alias": "error",
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/no-var-requires": "error",
        "@typescript-eslint/no-unused-vars": [2, {args: 'none'}],
        "@typescript-eslint/prefer-for-of": "error",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/prefer-namespace-keyword": "error",
        "@typescript-eslint/semi": [
            "error",
            "always"
        ],
        "@typescript-eslint/triple-slash-reference": "error",
        "@typescript-eslint/type-annotation-spacing": "error",
        "@typescript-eslint/unified-signatures": "error",
        "arrow-parens": [
            "error",
            "always"
        ],
        "camelcase": "off",
        "comma-dangle": [
            "error",
            "always-multiline"
        ],
        "complexity": "off",
        "constructor-super": "error",
        "curly": "error",
        "dot-notation": "error",
        "eol-last": "error",
        "guard-for-in": "error",
        "id-blacklist": [
            "error",
            "String",
            "Boolean",
            "Undefined",
        ],
        "id-match": "error",
        "import/no-extraneous-dependencies": [
            "error",
            {
                "devDependencies": true
            }
        ],
        "import/no-internal-modules": [
            "error",
            {
                "allow": [
                    "@testing-library/jest-dom/extend-expect",
                ]
            }
        ],
        "import/order": ["error", {"groups": ["external", "internal"], "alphabetize": {"order": "asc"}}],
        "max-classes-per-file": [
            "error",
            1
        ],
        "max-len": [
            "error",
            {
                "code": 180
            }
        ],
        "no-bitwise": "error",
        "no-console": "error",
        "no-debugger": "error",
        "no-duplicate-imports": "error",
        "no-empty": "error",
        "no-invalid-this": "off",
        "no-multiple-empty-lines": "error",
        "no-return-await": "error",
        "no-shadow": [
            "error",
            {
                "hoist": "all"
            }
        ],
        "no-trailing-spaces": "error",
        "no-undef-init": "error",
        "no-underscore-dangle": "error",
        "no-unsafe-finally": "error",
        "no-var": "error",
        "object-shorthand": "error",
        "one-var": [
            "error",
            "never"
        ],
        "prefer-arrow/prefer-arrow-functions": "error",
        "prefer-const": "error",
        "prefer-object-spread": "error",
        "quote-props": [
            "error",
            "consistent-as-needed"
        ],
        "radix": "off",
        "space-before-function-paren": [
            "error",
            {
                "anonymous": "never",
                "asyncArrow": "always",
                "named": "never"
            }
        ],
        "space-in-parens": [
            "error",
            "never"
        ],
        "spaced-comment": "error",
        "react/prop-types": [0],
        "react/jsx-boolean-value": 0
    },
    "settings": {
        "react": {
            "pragma": "React",
            "version": "detect"
        },
        "linkComponents": [
            "Hyperlink",
            {"name": "Link", "linkAttribute": "to"}
        ]
    }
};
